# Dragonfly

## Goal: A full-sized electric motorcycle

### Initial specifications

Subject to change.

* Front suspension: Hossack style front end
* Rear suspension: TBD
* Recumbent
* Electric
    * [ME1616 PMAC](http://www.motenergy.com/mepmwaco.html) motor
	* [Trampa VESC 100/250](https://trampaboards.com/vesc-100250--p-27368.html) motor controller
	* Batteries: 24 [Fortune 100AH](https://www.electriccarpartscompany.com/Fortune-100Ah-Aluminum-Encased-Battery) LiFePO4 cells, in series
	    * 300A peak
		* 74V nominal, 84V peak
		* 7.44KWh
	* BMS: [Chargery BMS24P-600 V4.05](https://www.chargerystore.com/index.php?route=product/product&path=20_60&product_id=108)
	* DC-DC: [Mean Well SD-500H-12](https://www.meanwell-web.com/en-gb/dc-dc-enclosed-converter-input-72-144vdc-output-sd--500h--12)
	* Charger: [Chargery C10325B](https://www.chargerystore.com/index.php?route=product/product&path=33&product_id=62)
* Drive train ratio: TBD

#### Donor parts:

|||
|--:|---|
|Front wheel| 2005 Suzuki GSXR 1000, 600mm diameter, dual Tokico brakes|
|Rear wheel| 2003 Suzuki GSXR 600, 630mm diameter|

## Build Log

[Build Log](build_log/index.md)


## Related projects

* [Dragonfly Precharge](https://gitlab.com/nuance-systems/dragonfly-precharge) EV precharge control circuit
* [Dragonfly EVSE Interface](https://gitlab.com/nuance-systems/dragonfly-evse-interface) Interface circuit to talk to an EVSE (Electric Vehicle Supply Equipment AKA "electric car charging station")
* [Dragonfly CAN Expander](https://gitlab.com/nuance-systems/dragonfly-can-expander) CAN bus GPIO + UART + SPI + I<sup>2</sup>C; the "Swiss Army Knife of CAN"
