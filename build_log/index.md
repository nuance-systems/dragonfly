# DRAGONFLY BUILD LOG

<a name="plan_a"></a>
## Front Forks Attempt #0: Failure ("Plan A")</a>
|  |  |  |
|--|--|--|
|Log 0| [Front fork uprights](0.md) | 5 Sept 2021|
|Log 1| [Welded Axle Clamps, Front Fork Uprights (Redux), Brake Caliper Test Fit](1.md) | 16 May 2022|
|Log 2| [Welded, Blended fork clamps](2.md) (failure noted here) | 31 May 2022|

## Front Forks Attempt #1: "Plan B"

|  |  |  |
|--|--|--|
|Log 3| [Front fork uprights: "Plan B"](3.md) | 20 July 2022|

## Front brake mounts and fork spacing

|  |  |  |
|--|--|--|
|Log 4| [Front brake mounts and fork spacing](4.md) | September 2022|
